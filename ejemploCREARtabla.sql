﻿/* hoja 7 - unidad 1 - modulo 3 */

  -- eliminar la base de datos si existe
  DROP DATABASE IF EXISTS hoja7unidad1modulo3;

  -- crear la base de datos si no existe
 CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3;

-- utilizar la base de datos
USE hoja7unidad1modulo3;
 
-- crear tabla empleado
 CREATE TABLE empleado(
    dni char(9),
    PRIMARY KEY (dni)
);

INSERT INTO empleado VALUES
  ('dni1'),
  ('dni2');


-- crear tabla departamento
CREATE TABLE departamento(
  CodDept varchar(15),
  PRIMARY KEY (CodDept)
  );

INSERT INTO departamento VALUES
  ('departamento1'),
  ('departamento2');


-- eliminar tabla
  DROP TABLE IF EXISTS pertenece;

-- crear tabla pertenece
  CREATE TABLE IF NOT EXISTS pertenece(
    departamento varchar(15),
    empleado char(9),
    PRIMARY KEY (departamento, empleado),
    CONSTRAINT uniqueEmpleado UNIQUE KEY(empleado),
    CONSTRAINT FKPerteneceEmpleado FOREIGN KEY(empleado)
      REFERENCES empleado(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKPerteneceDepartamento FOREIGN KEY(departamento)
      REFERENCES departamento(CodDept) ON DELETE CASCADE ON UPDATE CASCADE   
    );


-- crear tabla proyecto
 DROP TABLE IF EXISTS proyecto;
 CREATE TABLE IF NOT EXISTS proyecto(
    codigoProyecto varchar(15),
    PRIMARY KEY (codigoProyecto)
);


-- crear tabla trabaja
  CREATE TABLE IF NOT EXISTS trabaja(
    empleado char(9),
    proyecto varchar(15),
    fecha date,
    PRIMARY KEY (empleado, proyecto),
    CONSTRAINT FKTrabajaEmpleado FOREIGN KEY(empleado)
      REFERENCES empleado(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKTrabajaProyecto FOREIGN KEY(proyecto)
      REFERENCES proyecto(codigoProyecto) ON DELETE CASCADE ON UPDATE CASCADE   
    );