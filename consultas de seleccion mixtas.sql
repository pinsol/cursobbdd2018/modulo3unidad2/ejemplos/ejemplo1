﻿/* Consulta de seleccion 6.1 */
  SELECT COUNT(*)
    FROM vendedores v
    WHERE MONTH(v.FechaAlta)=2
;

/* Consulta de seleccion 6.2 */
  SELECT
  COUNT(*) 
  FROM
  vendedores v
  WHERE v.`Guap@` IS TRUE
  ;

  SELECT
    COUNT(*)
    FROM
    vendedores v
    WHERE v.`Guap@` IS FALSE
    ;
  
  SELECT (SELECT
  COUNT(*) 
  FROM
  vendedores v
  WHERE v.`Guap@` IS TRUE)+(SELECT
    COUNT(*)
    FROM
    vendedores v
    WHERE v.`Guap@` IS FALSE)
  ;

/* Consulta de seleccion 6.3 */
  SELECT MAX(p.Precio) FROM productos p;

  SELECT NomProducto FROM productos p WHERE p.Precio=(SELECT MAX(p.Precio) FROM productos p)
  ;

/* Consulta de seleccion 6.4 */
  SELECT
  AVG(p.Precio) 
  FROM
  productos p
  GROUP BY p.IdGrupo
  ;

/* Consulta de seleccion 6.5 */

  SELECT
    DISTINCT p.IdGrupo 
  FROM ventas v
    INNER JOIN productos p ON v.`Cod Producto` = p.IdProducto
  ;

  /* Consulta de seleccion 6.6 */
  SELECT
    DISTINCT p.IdGrupo 
  FROM productos p
    LEFT JOIN ventas v ON v.`Cod Producto` = p.IdProducto
    WHERE v.`Cod Vendedor` IS NULL
    ;

  /* Consulta de seleccion 6.7 */
    SELECT DISTINCT v.Poblacion FROM vendedores v WHERE v.`Guap@` IS TRUE;
    SELECT COUNT(*) FROM ( SELECT DISTINCT v.Poblacion FROM vendedores v WHERE v.`Guap@` IS TRUE) c1;

    SELECT COUNT(DISTINCT v.Poblacion) FROM vendedores v WHERE v.`Guap@` IS TRUE;

 /* Consulta de seleccion 6.8 */
SELECT COUNT(*), v.Poblacion FROM vendedores v WHERE v.EstalCivil='Casado' GROUP BY v.Poblacion;

SELECT
  Poblacion 
FROM vendedores v
WHERE v.EstalCivil='Casado'
  GROUP BY v.Poblacion
  HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT poblacion, COUNT(*) n FROM vendedores v WHERE v.EstalCivil='Casado' GROUP BY v.Poblacion) c1)
  ;

 /* Consulta de seleccion 6.9 */
  SELECT DISTINCT Poblacion FROM vendedores WHERE EstalCivil LIKE 'Casado';
  SELECT DISTINCT Poblacion FROM vendedores WHERE EstalCivil NOT LIKE 'Casado';

SELECT
  c1.Poblacion 
FROM
  (SELECT DISTINCT Poblacion FROM vendedores WHERE EstalCivil NOT LIKE 'Casado') c1
  LEFT JOIN
  (SELECT DISTINCT Poblacion FROM vendedores WHERE EstalCivil LIKE 'Casado') c2
  ON c1.Poblacion=c2.Poblacion
WHERE
  c2.Poblacion IS NULL
  ;

 /* Consulta de seleccion 6.10 */
SELECT DISTINCT v.`Cod Vendedor` FROM ventas v;
SELECT
  v.IdVendedor
FROM
  vendedores v
  LEFT JOIN (SELECT DISTINCT v.`Cod Vendedor` FROM ventas v) c1 ON v.IdVendedor=c1.`Cod Vendedor`
  WHERE c1.`Cod Vendedor` IS NULL
  ;

 /* Consulta de seleccion 6.11 */
SELECT
 DISTINCT v1.NombreVendedor 
FROM
 ventas v 
  JOIN
 vendedores v1 ON v.`Cod Vendedor` = v1.IdVendedor 
WHERE
 v.Kilos=(SELECT MAX(kilos) FROM ventas v1)
;