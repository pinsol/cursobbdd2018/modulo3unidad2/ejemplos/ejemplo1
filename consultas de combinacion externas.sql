﻿/* Consulta de combinacion externa 5.1 */
 SELECT
  DISTINCT c.nombre, c.edad 
 
 FROM
  ciclista c
    LEFT JOIN etapa e ON c.dorsal=e.dorsal

WHERE
 e.dorsal IS NULL;


/* Consulta de combinacion externa 5.2 */
 SELECT
  DISTINCT c.nombre, c.edad 
 
 FROM
  ciclista c
    LEFT JOIN puerto p ON c.dorsal=p.dorsal

WHERE
 p.dorsal IS NULL;


/* Consulta de combinacion externa 5.3 */
SELECT DISTINCT
  c.nomequipo
FROM ciclista c
  LEFT JOIN puerto p
    ON c.dorsal = p.dorsal
WHERE p.dorsal IS NULL;

SELECT
 DISTINCT e.director
FROM equipo e
WHERE e.nomequipo
IN (SELECT DISTINCT c.nomequipo FROM ciclista 
  c LEFT JOIN puerto p ON c.dorsal = p.dorsal
  WHERE p.dorsal IS NULL);



/* Consulta de combinacion externa 5.4 */
 SELECT
  DISTINCT c.dorsal, nombre
 
 FROM
  ciclista c
    LEFT JOIN lleva l ON c.dorsal=l.dorsal

WHERE
 l.dorsal IS NULL;


/* Consulta de combinacion externa 5.5 */
 SELECT
  DISTINCT c.dorsal, nombre
 
 FROM
  ciclista c
    LEFT JOIN lleva l ON c.dorsal=l.dorsal
    LEFT JOIN maillot m ON l.código=m.código

WHERE
 m.color='amarillo'
;


/* Consulta de combinacion externa 5.6 */
 SELECT
  DISTINCT e.numetapa
 
 FROM
  ciclista c
   LEFT JOIN puerto p ON c.dorsal=p.dorsal
   LEFT JOIN etapa e ON c.dorsal=e.dorsal
WHERE
  p.dorsal IS NULL
;


/* Consulta de combinacion externa 5.7 */
SELECT 
  AVG(e.kms) 
FROM
  etapa e
  LEFT JOIN puerto p ON e.numetapa = p.numetapa
;


/* Consulta de combinacion externa 5.8 */
SELECT 
  COUNT(*) 
FROM ciclista c
  LEFT JOIN etapa e ON c.dorsal = e.dorsal
WHERE 
e.dorsal IS NULL
;


/* Consulta de combinacion externa 5.9 */
SELECT
 DISTINCT c1.dorsal 
FROM
 (SELECT c.dorsal FROM ciclista c
 JOIN etapa e ON c.dorsal = e.dorsal) c1
 LEFT JOIN puerto p ON c1.dorsal = p.dorsal
WHERE
p.dorsal IS NULL
;

/* Consulta de combinacion externa 5.10 */

-- 
SELECT DISTINCT e.dorsal
 FROM etapa e
  LEFT JOIN puerto p ON e.numetapa = p.numetapa
WHERE
p.dorsal IS NULL
  ;

SELECT DISTINCT e.dorsal
 FROM etapa e
  INNER JOIN puerto p ON e.numetapa = p.numetapa
  ;

SELECT c1.dorsal 
FROM  
  (SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.dorsal IS NULL) c1
  LEFT JOIN    
  (SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa = p.numetapa) c2
ON c1.dorsal = c2.dorsal
  WHERE
  c2.dorsal IS NULL
  ;

