﻿/* Consulta de seleccion y totales 3.1 */
SELECT
DISTINCT
  edad

FROM ciclista

WHERE nomequipo = 'Banesto';


/* Consulta de seleccion y totales 3.2 */
SELECT
DISTINCT
  edad

FROM ciclista

WHERE nomequipo = 'Banesto'
OR nomequipo = 'Navigare';

-- usando clausula IN
SELECT
DISTINCT
  edad

FROM ciclista

WHERE 
 nomequipo IN ('Banesto','Navigare');


/* Consulta de seleccion y totales 3.3 */
SELECT
  dorsal

FROM ciclista

WHERE nomequipo = 'Banesto'
AND edad BETWEEN 25 AND 32;


/* Consulta de seleccion y totales 3.4 */
SELECT
  dorsal

FROM ciclista

WHERE nomequipo = 'Banesto'
OR edad BETWEEN 25 AND 32;


/* Consulta de seleccion y totales 3.5 */
SELECT
DISTINCT
  LEFT(nomequipo, 1) AS inicial

FROM ciclista

WHERE LEFT(nombre, 1) = "r";


/* Consulta de seleccion y totales 3.6 */
SELECT
  numetapa

FROM etapa

WHERE salida = llegada;


/* Consulta de seleccion y totales 3.7 */
SELECT
  numetapa,
  dorsal

FROM etapa

WHERE salida <> llegada;

-- conociendo el dorsal
  SELECT
    numetapa 
  FROM
  etapa
  
  WHERE
  salida<>llegada AND dorsal IS NOT NULL;


/* Consulta de seleccion y totales 3.8 */
SELECT
  nompuerto

FROM puerto

WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;


/* Consulta de seleccion y totales 3.9 */
SELECT
DISTINCT
  dorsal

FROM puerto

WHERE altura BETWEEN 1000 AND 2000
OR altura > 2400;


/* Consulta de seleccion y totales 3.10 */
SELECT
 COUNT(dorsal)

FROM etapa
 GROUP BY dorsal;


/* Consulta de seleccion y totales 3.11 */
SELECT
  COUNT(DISTINCT numetapa)

FROM puerto;


/* Consulta de seleccion y totales 3.12 */
SELECT
  COUNT(DISTINCT dorsal)

FROM puerto;


/* Consulta de seleccion y totales 3.13 */
SELECT
  numetapa,
  COUNT(*) npuerto

FROM puerto
GROUP BY numetapa;


/* Consulta de seleccion y totales 3.14 */
SELECT
  AVG(altura) alturaMedia

FROM puerto;


/* Consulta de seleccion y totales 3.15 */
SELECT
DISTINCT
  numetapa

FROM puerto
GROUP BY altura
HAVING AVG(altura) > 1500;


/* Consulta de seleccion y totales 3.16 */
SELECT
  COUNT(*)

FROM (SELECT DISTINCT
    numetapa
  FROM puerto
  GROUP BY altura
  HAVING AVG(altura) > 1500) c1;

/* Consulta de seleccion y totales 3.17 */
SELECT
  dorsal,
  COUNT(*) total

FROM lleva
GROUP BY dorsal;

/* Consulta de seleccion y totales 3.18 */
SELECT
DISTINCT
  dorsal,
  código,
  COUNT(*) total

FROM lleva
GROUP BY dorsal,
         código;


/* Consulta de seleccion y totales 3.19 */
SELECT
 numetapa,
  dorsal,
  COUNT(*) total

FROM lleva
GROUP BY dorsal,
         numetapa;


-- añadir nombre con innerjoin
SELECT
 DISTINCT numetapa, c1.dorsal, nombre, total 

FROM
 (SELECT DISTINCT numetapa, dorsal, COUNT(*) total FROM lleva GROUP BY dorsal, numetapa) c1
 INNER JOIN
  ciclista c ON c1.dorsal=c.dorsal;