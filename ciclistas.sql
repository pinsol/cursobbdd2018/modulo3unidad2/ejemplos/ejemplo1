-- ----------------------------------------------------------------------
-- MySQL Migration Toolkit
-- SQL Create Script
-- ----------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;

CREATE DATABASE IF NOT EXISTS `ciclistas`
  CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ciclistas`;
-- -------------------------------------
-- Tables

DROP TABLE IF EXISTS `ciclistas`.`ciclista`;
CREATE TABLE `ciclistas`.`ciclista` (
  `dorsal` SMALLINT(5) NOT NULL,
  `nombre` VARCHAR(30) NOT NULL,
  `edad` SMALLINT(5) NULL,
  `nomequipo` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`dorsal`),
  INDEX `equipociclista` (`nomequipo`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`equipo`;
CREATE TABLE `ciclistas`.`equipo` (
  `nomequipo` VARCHAR(25) NOT NULL,
  `director` VARCHAR(30) NULL,
  PRIMARY KEY (`nomequipo`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`etapa`;
CREATE TABLE `ciclistas`.`etapa` (
  `numetapa` SMALLINT(5) NOT NULL,
  `kms` SMALLINT(5) NOT NULL,
  `salida` VARCHAR(35) NOT NULL,
  `llegada` VARCHAR(35) NOT NULL,
  `dorsal` SMALLINT(5) NULL,
  PRIMARY KEY (`numetapa`),
  INDEX `ciclistaetapa` (`dorsal`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`lleva`;
CREATE TABLE `ciclistas`.`lleva` (
  `dorsal` SMALLINT(5) NOT NULL,
  `numetapa` SMALLINT(5) NOT NULL,
  `código` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`numetapa`, `código`),
  INDEX `ciclistallevar` (`dorsal`),
  INDEX `etapallevar` (`numetapa`),
  INDEX `maillotllevar` (`código`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`maillot`;
CREATE TABLE `ciclistas`.`maillot` (
  `código` VARCHAR(3) NOT NULL,
  `tipo` VARCHAR(30) NOT NULL,
  `color` VARCHAR(20) NOT NULL,
  `premio` INT(10) NOT NULL,
  PRIMARY KEY (`código`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `ciclistas`.`puerto`;
CREATE TABLE `ciclistas`.`puerto` (
  `nompuerto` VARCHAR(35) NOT NULL,
  `altura` SMALLINT(5) NOT NULL,
  `categoria` VARCHAR(1) NOT NULL,
  `pendiente` DOUBLE(15, 5) NULL,
  `numetapa` SMALLINT(5) NOT NULL,
  `dorsal` SMALLINT(5) NULL,
  PRIMARY KEY (`nompuerto`),
  INDEX `ciclistapuerto` (`dorsal`),
  INDEX `etapapuerto` (`numetapa`)
)
ENGINE = INNODB
CHARACTER SET utf8 COLLATE utf8_general_ci;



SET FOREIGN_KEY_CHECKS = 1;

-- ----------------------------------------------------------------------
-- EOF

