﻿/* Consulta de combinacion interna 4.1 */
 SELECT
  DISTINCT nombre, edad 
 
 FROM
  ciclista c
    JOIN etapa e ON c.dorsal=e.dorsal;


/* Consulta de combinacion interna 4.2 */
 SELECT
  DISTINCT nombre, edad 
 
 FROM
  ciclista c
    JOIN puerto p ON c.dorsal=p.dorsal;


/* Consulta de combinacion interna 4.3 */
 SELECT
  DISTINCT nombre, edad 
 
 FROM
  ciclista c
    JOIN etapa e ON c.dorsal=e.dorsal
    JOIN puerto p ON c.dorsal=p.dorsal;


/* Consulta de combinacion interna 4.4 */
 SELECT
  DISTINCT director
 
 FROM
  ciclista c
    JOIN equipo e ON c.nomequipo=e.nomequipo
    JOIN etapa e1 ON c.dorsal=e1.dorsal;

/* Consulta de combinacion interna 4.5 */
 SELECT
  DISTINCT l.dorsal, nombre
 
 FROM
  ciclista c
    JOIN lleva l ON c.dorsal=l.dorsal;


/* Consulta de combinacion interna 4.6 */
 SELECT
  DISTINCT l.dorsal, nombre
 
 FROM
  ciclista c
    JOIN lleva l ON c.dorsal=l.dorsal
    JOIN maillot m ON l.código=m.código

WHERE
 color='amarillo';


/* Consulta de combinacion interna 4.7 */
 SELECT
  DISTINCT c.dorsal
 
 FROM
  ciclista c
    JOIN lleva l ON c.dorsal=l.dorsal
    JOIN etapa e ON c.dorsal=e.dorsal;


/* Consulta de combinacion interna 4.8 */
 SELECT
  DISTINCT e.numetapa
 
 FROM
  ciclista c
    JOIN puerto p ON c.dorsal=p.dorsal
    JOIN etapa e ON c.dorsal=e.dorsal;


/* Consulta de combinacion interna 4.9 */
 SELECT
  DISTINCT e.kms
 
 FROM
  ciclista c
    JOIN puerto p ON c.dorsal=p.dorsal
    JOIN etapa e ON c.dorsal=e.dorsal

WHERE
 nomequipo='Banesto';


/* Consulta de combinacion interna 4.10 */
 SELECT
  COUNT(DISTINCT c.dorsal) nCiclistas
 
 FROM
  ciclista c
    JOIN etapa e ON c.dorsal=e.dorsal
    JOIN puerto p ON c.dorsal=p.dorsal;


/* Consulta de combinacion interna 4.11 */
SELECT
 DISTINCT nompuerto 
 
FROM
 ciclista c
    JOIN puerto p ON c.dorsal=p.dorsal

WHERE
 nomequipo='Banesto';

/* Consulta de combinacion interna 4.12 */
SELECT
  COUNT(DISTINCT e.numetapa) total
 
FROM
  ciclista c
    JOIN etapa e ON c.dorsal=e.dorsal
    JOIN puerto p ON c.dorsal=p.dorsal

WHERE
 nomequipo='Banesto'
 AND
 kms>200;