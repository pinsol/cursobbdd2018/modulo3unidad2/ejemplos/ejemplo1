﻿/* Consulta de totales 2.1 */
SELECT
 COUNT(*) AS total

FROM
 ciclista;


/* Consulta de totales 2.2 */
SELECT
 COUNT(*) AS `total en Banesto`

FROM
 ciclista

WHERE
  nomequipo LIKE 'Banesto';


/* Consulta de totales 2.3 */
SELECT
 AVG(edad) AS `edad media`

FROM
 ciclista;

/* Consulta de totales 2.4 */
SELECT
 AVG(edad) AS `edad media en Banesto`

FROM
 ciclista

WHERE
nomequipo LIKE 'Banesto';

/* Consulta de totales 2.5 */
SELECT
 AVG(edad) `edad media por equipo`, nomequipo

FROM
  ciclista
  GROUP BY nomequipo;

/* Consulta de totales 2.6 */
SELECT
 COUNT(*) total, nomequipo

FROM
 ciclista
 GROUP BY nomequipo;

/* Consulta de totales 2.7 */
SELECT
 COUNT(*) total

FROM
 puerto;

/* Consulta de totales 2.8 */
SELECT
 COUNT(*) total

FROM
 puerto

WHERE 
 altura>1500;

/* Consulta de totales 2.9 */
SELECT
 COUNT(*) AS total, nomequipo

FROM
 ciclista
 GROUP BY nomequipo

 HAVING total>4;

/* Consulta de totales 2.10 */
SELECT
 COUNT(*) AS total, nomequipo

FROM
 ciclista

WHERE
edad BETWEEN 28 AND 32

GROUP BY nomequipo

HAVING 
 total>4;

/* Consulta de totales 2.11 */
SELECT
 dorsal, COUNT(*) AS total

FROM
 etapa

  GROUP BY dorsal;

/* Consulta de totales 2.12 */
SELECT
 dorsal

FROM
 etapa

  GROUP BY dorsal

  HAVING  COUNT(*)>1;